// 2023-04-04 alexeit at home 3:10

#include "binary_tree.hpp"

int main()
{
  BinarySearchTree<int> bst;

  std::cout << "insert\n=======" << '\n';

  bst.insert(2);
  bst.insert(1);
  bst.insert(3);
  bst.print(std::cout);

  bst.insert(-1);
  bst.insert(0);

  bst.print(std::cout);

  bst.insert(4);
  bst.insert(5);

  bst.print(std::cout);

#if 0
  std::cout << "search\n=======" << '\n';

  for (int i = -10; i <= 10; ++i)
  {
    std::cout << i << (bst.iterativeSearch(i) ? " found" : " not found") << '\n';
  }
#endif

  return 0;
}